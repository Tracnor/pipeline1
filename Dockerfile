FROM httpd:2.4.51-bullseye
EXPOSE 80
WORKDIR /usr/local/apache2/htdocs
COPY . .
